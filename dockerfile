FROM mariadb:latest

ENV TZ=America/Bogota
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENV MYSQL_ROOT_PASSWORD=appgate
RUN sed -i 's/#bind-address=0.0.0.0/bind-address=0.0.0.0/g' "/etc/mysql/my.cnf"
COPY backup.sql /opt
RUN chmod +x /opt/backup.sql
COPY import.sh /docker-entrypoint-initdb.d
RUN chmod +x /docker-entrypoint-initdb.d/import.sh
EXPOSE 3306